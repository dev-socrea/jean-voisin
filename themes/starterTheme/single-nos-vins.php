<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <main role="main" class="w-100 main-content">
            <div class="background-full-width-nos-vins">
                <div class="container">
                    <div class="container-title-vin gothamb fs-40 ml-20 text-uppercase apparition">
                        <?php
                        $phrase = get_the_title();
                        $premierMotTitre = substr($phrase, 0, strpos($phrase, ' '));
                        $resteTitre = str_replace($premierMotTitre, '', $phrase);
                        echo ('<h1 class="text-right gothamb titre-single-nos-vins">' . $premierMotTitre . "</br>" . $resteTitre . '</h1>');
                        ?>
                    </div>
                    <div class="gothamb nombre-tuile-vin-single fw-800 d-flex">
                        3.1
                    </div>
                </div>
                <div class="trait-after-nombre-vin mb-50"></div>
                <div class="container pb-100">
                    <div class="d-inline-block">
                        <?php while ( have_rows('caracteristique') ) : the_row(); ?>
                            <div class="titre-caracteristique gothamb fs-20 fw-800 pr-100">
                                <span><?php the_sub_field('nom_caracteristique'); ?></span>
                            </div>
                            <div class="trait-caracteristique"></div>
                        <?php endwhile; ?>
                        <div class="titre-caracteristique gothamb fs-20 fw-800 pr-100">
                            <?php _e('Téléchargement') ?>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <?php while ( have_rows('caracteristique') ) : the_row(); ?>
                            <div class="valeur-caracteristique gothaml fs-20">
                                <?php the_sub_field('valeur_caracteristique'); ?>
                            </div>
                            <div class="trait-valeur"></div>
                        <?php endwhile; ?>
                        <div class="valeur-caracteristique gothaml fs-20">
                            <a download="<?php the_title(); ?>" href="<?php the_field('telechargement') ?>"><?php _e('Fiche PDF') ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="background-seconde-partie-single pb-100">
                <div class="d-flex container">
                    <div class="description-vin-single mt-100">
                        <?php wp_reset_query(); ?>
                        <?php the_content();?>
                    </div>
                    <img class="parallax ml-auto image-neg-single mr-100 image-bouteille-single" src="<?php the_field('image_bouteille'); ?>" alt="">
                    <div class="trait-vertical-single"></div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>

<?php get_header(); ?>

    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-lg-8 col-md-8 col-xs-8 main-content">
                <!-- section -->
                <section>

                    <?php get_template_part('categories/category', 'first-custom-post'); ?>

                    <h1><?php _e('Archives Second Custom Post', 'starterTheme'); ?></h1>

                    <div class="ajax">
                        <div class="tax">
                            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                                <?php get_template_part('loops/loop', 'ajax'); ?>
                            <?php endwhile; endif; ?>
                        </div>
                        <div class="more"></div>
                        <?php get_template_part('paginations/pagination', 'load-more'); ?>
                    </div>

                </section>
                <!-- /section -->
            </main>
            <div class="col-lg-4 col-md-4 col-xs-4 pull-left right-side-bar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        (function($){
            $(document).ready(function(){
                var taxFilter = '.term-filter',
                    loadMore  = '.load-more button',
                    triggers  = [taxFilter, loadMore];
                var postType  = <?=json_encode(obj_name())?>,
                    taxSlug   = <?=json_encode(tax_name())?>,
                    tmpName   = 'archive-second-custom-post.php',
                    nbPosts, termSlug, termId, selector;

                for(var i=0; i<triggers.length;i++){
                    $(triggers[i]).on('click', function(){
                        termSlug = $(this).attr('data-slug') != undefined ? $(this).attr('data-slug') : 'null';
                        termId   = $(this).attr('data-id')   != undefined ? $(this).attr('data-id')   : 'null';
                        selector = $(this).attr('id')   != undefined ? $(this).attr('id')   : 'null';
                        nbPosts  = $(this).attr('count-posts');
                        ajaxSort(postType, taxSlug, nbPosts, termSlug, termId, tmpName, selector);
                    })
                }
            });
        })(jQuery, this);
    </script>

<?php get_footer(); ?>
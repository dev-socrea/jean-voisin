<?php
ini_set('display_errors','on');
error_reporting(E_ALL);

//chargement manuel du coeur WordPress
require_once( '../../../wp-load.php' );

$id = $_POST['id'];

$the_post = get_post($id);

$the_title = $the_post->post_title;
$the_content = $the_post->post_content;
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'full' );

$image = $thumb[0];

$link = get_permalink($id);

?>



<?php
//Création d'un array transformé en JSON. Si la sytaxe JSON n'est pas correcte,
//Le javascript ne saura pas l'exploiter (ATTENTION aux erreurs php etc....)
$array[] = array('id'=> $id, 'title'=>$the_title, 'content'=>$the_content, 'image'=>$image, 'link'=>$link);
$array = json_encode($array);
echo $array;
 ?>

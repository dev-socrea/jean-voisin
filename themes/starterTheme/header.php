<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="https://plus.google.com/XXXXXXXX/" rel="publisher" />
        <link href="https://www.facebook.com/XXXXXXXX/" rel="publisher" />
        <link href="https://www.instagram.com/XXXXXXXX/" rel="publisher" />

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/manifest.json">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon.ico">
        <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/assets/img/icons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">

        <link href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">


		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>
		<script>
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri().'/assets'; ?>',
            tests: {}
        });
        </script>
	</head>

	<!-- BODY -->
    <?php $pluginsActive = (get_option('active_plugins'));
    if ( in_array( 'sitepress-multilingual-cms/sitepress.php', $pluginsActive)) { ?>
    <body <?php body_class(ICL_LANGUAGE_CODE); ?>>
	<?php } else { ?>
    <body <?php body_class(); ?>>
    <?php } ?>
        <!-- loader -->
        <div id="page-loader"></div>
    	<div class="wrapper anim-300">
    		<!-- header -->
    		<header class="header fixed-top clearfix anim-300" role="banner">
                <!-- <div id="header-sticky" class="anim-300"> -->
                    <!-- nav -->
                    <!-- <div class="container-fluid  menu-container anim-300"> -->
                        <!-- responsive nav -->
                        <!-- <nav class="nav-mobile" role="navigation"> -->
                            <?php //wp_nav_menu( array( 'theme_location' => 'burger-menu' ) ); ?>
                        <!-- </nav> -->
                        <!-- /responsive nav -->
                        <?php //require 'includes/header_left.php'; ?>
                        <?php //require 'includes/header_center.php'; ?>
                    <!-- </div> -->
                    <!-- /nav -->
					<div class="menu-vertical no-lazy anim-300">
						<div class="container-menu-vertical">
							<a href="<?php echo home_url(); ?>">
								<div class="logo-menu-vertical no-lazy text-center">
									<img src="<?php echo("/jean-voisin/wp-content/themes/starterTheme/assets/img/v-menu.svg");?>" alt="Chateau Jean Voisin">
								</div>
							</a>
							<div class="menu-btn">
								<div class="container-btn-menu">
									<menu>
										<span class="d-block gothamb fs-16">ME</span>
										<span class="d-block gothamb fs-16">NU</span>
									</menu>
								</div>
								<!-- <div class="container-btn-menu-2">
									<div class="rond-btn-menu"></div>
									<div class="rond-btn-menu"></div>
									<div class="rond-btn-menu"></div>
								</div> -->
							</div>
							<div class="langue-menu text-center">
								FR <i class="fa fa-caret-up" aria-hidden="true"></i>
							</div>
						</div>
					</div>
					<div class="container-border-menu-open nav-mobile">
						<div class="container-menu-open">
							<div class="close-menu-open menu-btn">
								<i class="fa fa-times" aria-hidden="true"></i>
							</div>
							<div class="content-titre-menu-open">
								<div class="barre-vertical">
								</div>
								<div class="container-logo-menu-open no-lazy">
									<img class="apparition" src="<?php echo("/jean-voisin/wp-content/themes/starterTheme/assets/img/logo-menu-open.svg");?>" alt="Chateau Jean Voisin">
									<h3 class="apparition din text-uppercase text-center titre-menu-open">Saint-émilion grand cru</h3>
								</div>
							</div>
							<div class="mt-100">
								<div class="row">
									<nav class="nav flex main-navigation-zone anim-300-no-ease text-center" role="navigation">
										<?php wp_nav_menu( array( 'theme_location' => 'burger-menu' ) ); ?>
									</nav>
								</div>
							</div>
							<div class="langue-menu-open text-center apparition">
								FR <i class="fa fa-caret-up" aria-hidden="true"></i>
							</div>
							<div class="sous-menu-open">
								<?php wp_nav_menu( array( 'theme_location' => 'sous-menu' ) ); ?>
							</div>
						</div>
					</div>
    			</div>
            </header>
    		<div id="top" class="sticky-spacing anim-300"></div>
    		<div class="clearfix"></div>
    		<!-- /header -->

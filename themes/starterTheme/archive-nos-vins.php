<?php get_header(); ?>

    <div class="container-fluid">
        <div class="row">
            <main role="main" class="w-100 main-content">
                <!-- section -->
                <section>
                    <div class="container-titre-vins">
                        <h1 class="gothamb fw-800 text-right pt-100 pr-100 text-uppercase apparition"><?php _e('Nos Vins') ?></h1>
                    </div>
                    <?php $compt = 0; ?>
                    <?php $loop = new WP_Query( array( 'post_type' => 'nos-vins', 'posts_per_page' => '10' )); ?>
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <?php $compt++; ?>
                        <div class="tuile-vin pb-100">
                            <div class="nombre-tuile-vin gothamb fw-800 d-flex container apparition">
                                <span>3.<?php echo $compt; ?></span>
                            </div>
                            <div class="container-image-tuile-vin no-lazy d-flex parallax apparition">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="Château Jean Voisin">
                            </div>
                            <div class="d-flex">
                                <div class="trait-after-nombre-vin align-middle apparition"></div>
                                <span class="fs-22 text-white gotamb text-uppercase align-middle ml-15 texte-trait-vin apparition">
                                    <?php
                                    if (get_field('edition_limitee')) {
                                        if (in_array('edition', get_field('edition_limitee'))) {
                                            _e('édition limitée');
                                        }
                                    }
                                     ?>
                                </span>
                            </div>
                            <div class="description-tuile-vin d-flex pt-50 container">
                                <a href="<?php the_permalink(); ?>" class="btn-voir-vin text-uppercase anim-300 apparition">
                                    <span class="align-middle"><?php _e("Voir <strong>le vin</strong>") ?></span>
                                </a>
                                <div class="container-title-single-vin gothaml fs-40 ml-20 text-uppercase apparition">
                                    <?php
                                    $phrase = get_the_title();
                        			$premierMotTitre = substr($phrase, 0, strpos($phrase, ' '));
                                    $resteTitre = str_replace($premierMotTitre, '', $phrase);
                                    echo ('<h2 class="">' . $premierMotTitre . "</br>" . $resteTitre . '</h2>');
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                </section>
                <!-- /section -->
            </main>
        </div>
    </div>

<?php get_footer(); ?>

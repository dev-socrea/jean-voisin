<?php
if( have_rows('flexible_content') ):
     // loop through the rows of data

    while ( have_rows('flexible_content') ) : the_row();

        // COLONNES
        if( get_row_layout() == 'container_fluid' ):
        	// choix du background
			$backgroundType = get_sub_field('check_radio');

			if ($backgroundType == 'Image'){
				$style = 'background-image: url(' . get_sub_field('image_de_fond') . ')';
			} else { //it's a color
				$style = 'background-color:' . get_sub_field('couleur_de_fond');
			}

				// choix du type de container
				$containerType = get_sub_field('choix_container');

				if ($containerType == 'Avec marges'){
					$container = 'container';
				}else if ($containerType == 'Sans marges'){
					$container = 'container-fluid';
				} else {
                    $container = 'container-fluid';
                    $containerchild = 'container';
                }
			?>
			<div class="<?php echo $container; ?>" >
				<div class="row" style="<?php echo $style;?>">
                    <div class="<?php echo $containerchild;?>">
				<?php
	        		// check if the flexible content field has rows of data
	        		if( have_rows('container_fluid_child') ):
	        		     // loop through the rows of data
	        		    while ( have_rows('container_fluid_child') ) : the_row();
	        		        if( get_row_layout() == 'colonnes' ):?>
								<?php

								$nb_colonnes = get_sub_field('nb_colonnes');

								switch ($nb_colonnes) {
									case '1 colonne':
										echo "<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">" . get_sub_field('colonne1') . "</div>";
										break;

									case '2 colonnes':
										echo
										"<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">" . get_sub_field('colonne1') . "</div>
										<div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">" . get_sub_field('colonne2') . "</div>";
										break;

									case '3 colonnes':
										echo
										"<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\">" . get_sub_field('colonne1') . "</div>
										<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\">" . get_sub_field('colonne2') . "</div>
										<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\">" . get_sub_field('colonne3') . "</div>";
										break;

									case '4 colonnes':
										echo
										"<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-12\">" . get_sub_field('colonne1') . "</div>
										<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-12\">" . get_sub_field('colonne2') . "</div>
										<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-12\">" . get_sub_field('colonne3') . "</div>
										<div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-12\">" . get_sub_field('colonne4') . "</div>";
										break;

									case '5 colonnes':
										echo
										"<div class=\"col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-12\">" . get_sub_field('colonne1') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne2') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne3') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne4') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne5') . "</div>";
										break;

									case '6 colonnes':
										echo
										"<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne1') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne2') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne3') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne4') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne5') . "</div>
										<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-12\">" . get_sub_field('colonne6') . "</div>";
										break;

									default:
										echo "Il n'y a pas de contenu à afficher";
								}
                            endif;
                            if( get_row_layout() == 'marge' ):
                                $taille_marge = get_sub_field('taille_marge').'px';?>
                                <div style= "width:100%; height:<?php echo $taille_marge; ?>;">

                                </div>
                            <?php endif;
	        		    endwhile;
	        		endif;?>

                </div>
				</div>
			</div>
	    <?php endif;


        // IMAGE TRANSITION
        if( get_row_layout() == 'image_transition' ):
            $imgTrans = get_sub_field('choix_image_de_transition');
			$image_de_transition = 'background-image: url(' . get_sub_field('image_de_transition') . ')';
            $taille_image = get_sub_field('taille_transition').'px';

            $parallax = get_sub_field('parallax');
			if ($parallax == 'Oui') {
				$style_parallax = 'background-attachment: fixed;';
			} else {
				$style_parallax = '';
			}
            ?>

            <div class="image_trans" style= "width:100%; height:<?php echo $taille_image;?>; <?php echo $style_parallax;?> background-repeat: no-repeat; background-position: center; background-size: cover;<?php echo $image_de_transition;?> " >
            </div>
            <?php
        endif;


        // ESPACE
        if( get_row_layout() == 'espace' ):
            $espace = get_sub_field('espace');
            $taille_espace = get_sub_field('taille_espace');
            $taille_finale = $taille_espace;
            ?>

            <div class="espace" style= "width:100%; height:<?php echo $taille_finale;?>px;" >
            </div>
            <?php
        endif;

            if( have_rows('image_texte') ):
                 // loop through the rows of data
                while ( have_rows('image_texte') ) : the_row();
                    if( get_row_layout() == 'ligne_photo+texte' ):
            $imgurl = get_sub_field('image');
            $texteimg = get_sub_field('texte'); ?>

					<div class="ligne_photo_texte vertical-center no-flex">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-0 ">
							<img src="<?php echo $imgurl; ?>" alt="">
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="text-container">
								<?php echo $texteimg; ?>
							</div>
						</div>
					</div>
            <?php
        endif;
    endwhile;
endif;

	    endwhile;
	endif;
	?>

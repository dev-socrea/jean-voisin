<div class="row logo-center anim-300">
    <div class="col-lg-12 col-md-12 col-xs-12 text-center anim-300">
        <!-- logo -->
        <div class="logo anim-300">
            <a href="<?php echo home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="Logo" class="logo-img anim-300">
            </a>
        </div>
        <!-- /logo -->
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12 text-center">
        <!-- nav -->
        <nav class="nav main-navigation-zone anim-300" role="navigation">
            <?php main_nav(); ?>
        </nav>
        <!-- /nav -->
    </div>
</div>
<!-- section -->
<section>
    <div class="container-fluid">
        <div class="row">
            <?php if (has_post_thumbnail()):
                $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); $thumb = $thumb[0]; ?>
            <?php else:
                $thumb = get_template_directory_uri().'/assets/img/single-default.jpg';
            endif; ?>
            <div class="col-lg-12 col-md-12 col-md-12 col-xs-12 wide-picture" style="background-image: url('<?=$thumb?>')">
                <div class="actu-titles vertical-center">
                    <div class="logo-actu">
                        <div class="actu-title">
                             <?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/devices-detect.php");?>
                                 <?php if ($tablet_browser > 0) {
                                   // do something for TABLET devices
                                    ?> 
                                    
                                    <?php
                                }
                                else if ($mobile_browser > 0) {
                                   // do something for MOBILE devices
                                  ?>
                                    <?php
                                }
                                else {
                                   // do something for EVRYTHING else
                                ?>
                                   <h1><?php the_title(); ?></h1>
                                <?php
                            }?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /section -->
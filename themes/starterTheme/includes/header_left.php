<div class="row menu-section logo-left anim-300-no-ease align-items-center p-15">
    <div class="col-3 anim-300-no-ease container-logo-menu">
        <!-- logo -->
        <div class="logo anim-300-no-ease no-lazy">
            <a href="<?php echo home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="Logo" class="no-lazy logo-img anim-300-no-ease">
            </a>
        </div>
    </div>
    <div class="col-auto ml-auto anim-300-no-ease large-menu align-items-center p-r-40">
        <!-- nav -->
        <nav class="menu-principal large-menu nav d-flex main-navigation-zone anim-300-no-ease" role="navigation">
           <?php main_nav(); ?>
        </nav>
    </div>
    <div id="menu-btn" class="col-auto  align-items-center">
        <span class="ubuntu f-s-15">MENU</span>
          <button>
            <span></span>
            <span></span>
            <span></span>
          </button>
    </div>
</div>

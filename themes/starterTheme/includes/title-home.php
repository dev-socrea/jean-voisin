
  <?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/devices-detect.php");?>
      <?php if ($tablet_browser > 0) {
        // do something for TABLET devices
         ?>

         <?php
     }
     else if ($mobile_browser > 0) {
        // do something for MOBILE devices
        echo "mobile";
       ?>
         <?php
     }
     else { ?>
       <section>
             <div class="home_slider">
               <?php while ( have_rows('slider_home') ) : the_row();
                 if( get_row_layout() == 'slide' ):
                   $titre = get_sub_field('titre');
                   $image_url = get_sub_field('image');
                   $description = get_sub_field('description');
                   $lien = get_sub_field('lien');
                 endif;?>
                 <article class="d-flex col-12 recent-post-nav no-padding mosaique" style="height:450px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(<?php echo $image_url ?>);">
                   <div class="slider-title-content">
                     <div class="ml-auto container">
                         <div style="margin-top: 135px;" class="m-l-10 text-right slider-title slideLeft">
                           <?php echo $titre; ?>
                         </div>
                       <div class="text-right" style="text-align: right; padding-left: 40%;"><?php echo $description; ?></div>
                       <div class="text-right btn_slider">
                         <a href="<?php echo $lien; ?>"><b>Voir Plus</b></a>
                       </div>
                     </div>
                   </div>
                 </article>
               <?php endwhile;?>
             </div>
         </section>
         <?php
        // do something for EVRYTHING else
        echo "desktop";
     ?>
     <?php
  }?>

<?php get_header(); ?>
<div class="container-fluid fil-dariane">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 no-padding flex">
				<h1>
					<?php _e('Latest Posts', 'starterTheme'); ?>
				</h1>
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 no-padding flex">
				<?php social_media('google'); social_media('twitter'); social_media('facebook'); ?>
        <form>
          <input type="text" size="30" onkeyup="showResult(this.value)">
          <div id="livesearch"></div>
        </form>
			</div>
		</div>
	</div>
</div>

<div class="container nopadding">
  <div class="row">
    <main role="main" class="main-content nopadding">
			<?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/title-single.php");?>
      <!-- section -->
      <section>
          <?php if (have_posts()): while (have_posts()) : the_post(); ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class('single-poste-content'); ?> >
                  <div class="col-lg-9 col-md-12 col-xs-12 article-complet">
                    <div class="">
                      <h1 class="title-actu-single"><?php the_title(); ?></h1>
                      <span>
                        <span><i class="fa fa-calendar-o"></i> <?php _e('Published on: ', 'starterTheme') ?></span>
                        <i><?php the_time('j F Y'); ?> </i>
                      </span>
                      <span class="m-l-15"><i class="fa fa-tag" aria-hidden="true"></i>
                        <?php
                        foreach((get_the_category()) as $category) {
                          if ($category->cat_name != 'slider') {
                            echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                          }
                        }?>
                      </span>
                      <div class="single3-img-container col-lg-12 col-md-12 col-xs-12 no-padding">
                        <?php if(has_post_thumbnail()){ // Check if thumbnail exists ?>
                          <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                          <div  class="single3-img m-b-30 m-t-30" style="background-image: url('<?php echo $thumb['0'];?>')">
                          </div>
                          <?php }else{ ?>
                            <div class="single3-img m-b-30 m-t-30" style="background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')"></div>
                            <?php } ?>
                            <div class="single3-content">
                              <?php the_content(); ?>
                            </div>
                            <?php social_media('google'); social_media('twitter'); social_media('facebook'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-12 col-xs-12 pull-left right-side-bar p-t-50">
                    <h2>
            					<?php _e('Latest Posts', 'starterTheme'); ?>
            				</h2>
                    <?php
                    $args = array( 'numberposts' => '4' );
                    $recent_posts = wp_get_recent_posts( $args );
                    foreach( $recent_posts as $recent ){
                      $excerpt = $recent["post_content"];
                      $excerpt = substr($excerpt, 0, 75);
                      $excerpt = $excerpt.'... <a href="'.get_permalink($recent["ID"]).'">'. __('View Article', 'starterTheme') .'</a>';
                      $background = wp_get_attachment_image_src( get_post_thumbnail_id( $recent["ID"] ), 'full' );
                        echo '
                        <a href="' . get_permalink($recent["ID"]) . '">
                        <article class="m-t-20 flex align-end col-lg-12 col-md-12 col-xs-12 anim-300 recent-post-nav no-padding mosaique" style="height:200px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(' . $background[0] . ');">
                          <div class="single3-titre-content">
                            <div class=" m-l-auto">
                              <a href="' . get_permalink($recent["ID"]) . '" class="text-uppercase text-white">
                                <div class="m-l-10 single3-title">'
                                  . $recent["post_title"].'
                                </div>
                              </a>
                              <div class="cat-mosaique m-l-10">' . get_the_category( $recent["ID"] )[0]->name . '</div>
                            <div>
                          </div>
                        </article> </a>';
                    }

                    wp_reset_query();

                  ?>
                      </div>
                    <!-- article -->

                </article>
              <?php // get_template_part('paginations/pagination', 'nextprev'); ?>
            <?php endwhile; ?>
          <?php else: ?>
            <!-- article -->
            <article>
              <h1><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h1>
            </article>
            <!-- /article -->
          <?php endif; ?>
        </div>
      </section>
      <!-- /section -->
    </main>
  </div>
</div>

<?php get_footer(); ?>

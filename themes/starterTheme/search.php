<?php get_header(); ?>

<div class="container-fluid titre-fil">
	<div class="container">
		<div class="row">
			<div class="col-12 p-0 d-flex">
				<h1><?php echo sprintf( __( '%s Search Results for ', 'starterTheme' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<main role="main">
		<div class="row">
			<section>
				<div class="col-9 col-xs-12 p-0 news-container">
					<?php get_template_part('loops/loop'); ?>
				</div>
			</section>
			<!-- /section -->
			<div class="col-3 hidden-xs pull-left right-side-bar pt-50">
		        <?php get_sidebar(); ?>
		    </div>
		</div>
		<div class="row">
			<div class="col-9 pagi text-center">
				<?php get_template_part('pagination');?>
			</div>
		</div>
	</main>
</div>
<?php get_footer(); ?>

<!-- section -->
<section>
    <h1><?php _e('Custom Categories for ', 'starterTheme'); post_type_archive_title(); ?></h1>
    <?php
    $terms = get_terms(tax_name());
    echo "<a href='#' class='term-filter' count-posts='".current_total()."'><h4>"; _e('All', 'starterTheme'); echo "</h4></a>";
    foreach ($terms as $term){
        $term_link = get_term_link($term);
        echo "<a href='#' class='term-filter' count-posts='".$term->count."' data-id='".$term->term_id."' data-slug='".$term->slug."'><h4>".$term->name."</h4></a>";
    }
    ?>
</section>
<!-- /section -->
<?php /* Template Name: Contact5 */ get_header(); ?>
<main role="main" class="main-content">
	<?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/title.php"); ?>
	<div class="container-fluid">
			<!-- section -->
			<section>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="row" >
						<div class="col-12 contact5-0">
							<h1 class="text-center mt-30 mb-30"><?php the_title(); ?></h1>
						</div>
					</div>
					<div class="row">
						<div class="col-7 hidden-xs contact5-1">
							<div class="table mx-auto text-container-fluid">
								<span >Contact</span>
								<i class="fa fa-paper-plane-o pl-30" aria-hidden="true"></i>
							</div>
						</div>
						<div class="col-5 col-xs-12 contact5-2">
							<span><b>+33</b> 07 00 00 00 00</span><br>
							<span><b>+33</b> 05 00 00 00 00</span>
						</div>
					</div>

					<div class="row">
						<div class="col-12 contact5-3 p-0">
							<div id="map"></div>
							<div class="col-3 hidden-xs p-30 d-flex contact5-4">
								<div class="ml-auto mr-20">
									<h3>Adresse</h3>
									<span>3616 Jones Street</span><br>
									<span>Fort Worth,</span><br>
									<span>TX 76107</span>
								</div>
								<div>
									<h3>Adresse 2</h3>
									<span>3616 Jones Street</span><br>
									<span>Fort Worth,</span><br>
									<span>TX 76107</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row contact5-5">
						<div class="col-8 col-sm-12 col-xs-12 pt-30 form-contact5">
							<h3 class="text-center pb-20">Parlez nous de votre projet</h3>
							<?php the_content(); ?>


						</div>
					</div>


				</article>
				<!-- /article -->
			<?php endwhile; ?>

			<?php else: ?>
				<!-- article -->
				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
				</article>
				<!-- /article -->
			<?php endif; ?>
			</section>
			<!-- /section -->

			    <script async defer
			      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYM2FZ6PBFvla3XFMkE6xALHBw2KPY3LY&callback=initMap">
			    </script>
			</div>
</main>

<!-- /container-fluid -->
<?php get_footer(); ?>

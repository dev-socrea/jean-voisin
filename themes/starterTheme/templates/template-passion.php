<?php /* Template Name: La passion */ get_header(); ?>
<div class="container-fluid">
	<div class="container">
		<div class="row justify-content-between">
			<div class="numero-page-un gothamb text-center text-gold-light col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
				<div class="nombre-page-chateau">
					<div class="trait-nombre apparition">
					</div>
					<p class="apparition passion-nb-page">2</p>
					<div class="trait-nombre2 apparition">
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
				<div class="container-v-chateau pt-80 text-right apparition">
				</div>
				<div class="d-flex apparition">
					<h1 class="text-uppercase ml-auto mt-200 ls-0">
						<span class="fs-50 gothamb text-grey-light"><?php the_title(); ?></span>
						<br><span class="fs-55 fw-600 gothamb text-gold">VIN - AUTOMOBILE</span>
					</h1>
				</div>
				<div class="description-chateau text-right mt-80 mb-80">
					<h2 class="gothamb text-grey-light fs-26 after-titre apparition"><?php the_field('titre_partie1') ?></h2>
					<div class="fs-17 gothamb apparition"><?php the_field('texte_partie1') ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid passion-part2 mt-200">
	<div class="container pt-100 pb-80">
		<div class="row justify-content-between">
			<div class="col-xl-5 col-lg-5 col-12 text-right">
				<h3 class="text-gold2 fs-22 gothamb fw-700 after-titre"><?php the_field('titre_partie2'); ?></h3>
				<div class="gothamb">
					<?php the_field('texte_partie2'); ?>
				</div>
			</div>
			<div class="col-xl-5 col-lg-5 col-12 passion-part2-after parallax">
				<img class="passion-img-part2" src="<?php the_field('img_partie2'); ?>" alt="saint émilion grand cru">
			</div>
		</div>
	</div>
</div>
<div class="container-fluid passion-part3" style="background: -webkit-linear-gradient(rgba(35,35,35,0.82) 50%, #313131 79%);
  background: -o-linear-gradient(rgba(35,35,35,0.82) 50%, #313131 79%);
  background: linear-gradient(rgba(35,35,35,0.82) 50%, #313131 79%),
  url(<?php the_field('bkg_partie3'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-12 text-right">
				<h3 class="text-white text-uppercase gothamb fw-700 fs-55 after-titre2 mt-150"><?php the_field('titre_partie3'); ?></h3>
			</div>
			<div class="col-5 gothamb text-white ml-auto text-right">
				<?php the_field('texte_partie3'); ?>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid passion-part4" style="background: -webkit-linear-gradient(0deg, rgba(35,35,35,0) 15%, rgba(35,35,35,0.82) 50%, #313131 79%);
  background: -o-linear-gradient(0deg, rgba(35,35,35,0) 15%, rgba(35,35,35,0.82) 50%, #313131 79%);
  background: linear-gradient(0deg, rgba(35,35,35,0) 15%, rgba(35,35,35,0.82) 50%, #313131 79%),
  url(<?php the_field('bkg_partie4'); ?>);">
  	<div class="container">
		<div class="row justify-content-around">
			<div class="col-xl-9 col-lg-9 col-12">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/voiture-voisin-passion-saint-emilion-grand-cru.png" alt="">
				<!-- <div class="col-xl-10 col-lg-10 col-12 border-img-part3"></div> -->
			</div>
		</div>
  	</div>
		<div class="col-xl-6 col-lg-6 col-12 p-30 bkg-gold ml-auto text-right passion-legend-part3">
			<div class="text-container gothamb fs-17 fw-700 p15">
				<?php the_field('legende_partie3'); ?>
			</div>
		</div>

	<div class="container mt-80">
		<div class="row justify-content-around ">
			<?php
			if( have_rows('content_partie4') ):
			    while ( have_rows('content_partie4') ) : the_row();
			    ?>
			    <div class="col-xl-4 col-lg-4 col-12 passion-content-part4">
			        <h3 class="text-gold2 gothamb fw-700 fs-90 mb-50"><?php the_sub_field('titre'); ?></h3>
			        <div class="fs-17 text-white"><?php the_sub_field('texte'); ?></div>
				</div>
				<?php
			    endwhile;
			else :
			endif;
			?>
		</div>
	</div>
</div>
<div class="container-fluid passion-part4bis">
	<div class="container">
		<div class="row passion-img-part4">
			<?php
			if( have_rows('content_img_part4') ):
			    while ( have_rows('content_img_part4') ) : the_row();
			    ?>
			    <div class="col-xl-2 col-lg-2 col-12">
			    	<img src="<?php the_sub_field('img') ;?>" alt="">
				</div>
				<?php
			    endwhile;
			else :
			endif;
			?>
		</div>
	</div>
</div>
<div class="container-fluid passion-part5">
	<div class="container pt-50	pb-80">
		<div class="row justify-content-around">
			<?php
			if( have_rows('content_partie5') ):
			    while ( have_rows('content_partie5') ) : the_row();
			    ?>
				<div class="col-xl-5 col-lg-5 col-12 text-right">
					<h3 class="text-gold2 fs-22 gothamb fw-700"><?php the_sub_field('titre'); ?></h3>
					<h4 class="text-gold2 fs-22 gothamb fw-700 after-titre"><?php the_sub_field('subtitle'); ?></h4>
					<div class="gothamb">
						<?php the_sub_field('texte'); ?>
					</div>
				</div>
				<div class="col-xl-5 col-lg-5 col-12 passion-part2-after parallax">
					<img class="passion-img-part5" src="<?php the_sub_field('img'); ?>" alt="saint émilion grand cru">
				</div>
				<?php
			    endwhile;
			else :
			endif;
			?>
		</div>
	</div>
</div>
<div class="container-fluid passion-part5bis" style="background: url(<?php the_field('bkg_partie5'); ?>);">
</div>

<?php get_footer('footer'); ?>

<?php /* Template Name: Prestation2 */ get_header(); ?>
<main role="main" class="main-content">
	<?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/title.php");?>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<div class="container p-0">
		<!-- <div class="row presta-title">
			<h1><?php the_title() ?></h1>
			<?php the_content('')?>
		</div> -->
			<div class="row align-items-center no-flex">
				<div class="col-3 col-xs-12 presta-slider">
					<?php
					if( have_rows('prestations') ):
						while ( have_rows('prestations') ) : the_row();?>
							<div class="presta-2">
								<div class="slide">
									<div class="d-block">
										<img src="<?php the_sub_field('slide');?>" alt="">
									</div>
								</div>
							</div>
						 <?php   endwhile;
					else :
					endif;
					?>
				</div>
				<div class="col-9 col-xs-12 descriptif">
					<?php
					if( have_rows('prestations') ):
						while ( have_rows('prestations') ) : the_row();?>
							<div class="descriptif-content col-12">
								<?php the_sub_field('description'); ?>
							</div>
						 <?php   endwhile;
					else :
					endif;
					?>
				</div>
			</div>
		<?php endwhile; ?>
		<?php else: ?>
			<!--  -->
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
			</article>
		<?php endif; ?>

	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>

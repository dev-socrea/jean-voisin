<?php /* Template Name: AjaxBlog */ get_header(); ?>
<script type="text/javascript">
//Fonction permettant de créer la requete et de la rendre compatible avec tous les navigateurs.
function getXMLHttpRequest() {
  var xhr = null;

  if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
      try {
        xhr = new ActiveXObject("Msxml2.XMLHTTP");
      } catch(e) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      }
    } else {
      xhr = new XMLHttpRequest();
    }
  } else {
    alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
    return null;
  }

  return xhr;
}
</script>
<script type="text/javascript">
//Fonction permettant d'envoyer une requete et de l'envoyer sur une fonction de "callback"
//La fonction de callback s'éxécute lorsque le script a recu la réponse du serveur.
  function getArticle(id, callback){

    var xhr = getXMLHttpRequest();

    //la requete est envoyée au fichier ajax-blog.php situé dans 'starterTheme'
    xhr.open("POST", "<?= get_template_directory_uri(); ?>/ajax-blog.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send('id=' + id);

    xhr.onreadystatechange = function(){
      if(xhr.readyState == 4 && xhr.status <= 200){
        //La fonction de callback avec comme parametre la réponse
        callback(xhr.response);
      }
    }
}

//Fonction permettant la mise en page des donnees récupérées
  function displayArticle(data){
    document.getElementById('popup-article').innerHTML = '<button type="button" onclick="close_popup()" style="z-index: 99999999" name="close">fermer</button>';
    document.getElementById('popup-article').style.opacity = '0';
    document.getElementById('popup-article').style.display = 'flex';
    document.getElementById('popup-article').style.opacity = '1';
    console.log(data);
    //JSON.parse sert à transformer le JSON recu en objet javascript
    data = JSON.parse(data);
    var title = document.createElement('h2');
    title.textContent = data[0].title;
    document.getElementById('popup-article').appendChild(title);

    var content = document.createElement('p');
    content.textContent = data[0].content;
    document.getElementById('popup-article').appendChild(content);

    var image = document.createElement('img');
    image.src = data[0].image;
    image.className = "single-ajax-image"
    document.getElementById('popup-article').appendChild(image);

    window.history.pushState('page2', 'Title', data[0].link);
  }

  document.getElementById('close-popup').addEventListener('click', function(){
    //console.log('click');
    document.getElementById('popup-article').style.opacity = '0';
    document.getElementById('popup-article').style.display = 'block!important';
  });

//fonction permettant la fermeture du popup
  function close_popup(){
    //console.log('click');
    document.getElementById('popup-article').style.opacity = '0';
    document.getElementById('popup-article').style.display = 'none';
  }
</script>
 <?php $args = array(
'offset'           => 0,
'category'         => '',
'category_name'    => '',
'orderby'          => 'date',
'order'            => 'DESC',
'include'          => '', 'exclude'          => '',
'meta_key'         => '',
'meta_value'       => '',
'post_type'        => 'post', 'post_mime_type'   => '',
'post_parent'      => '',
'author'       => '',
'post_status'      => 'publish',
'suppress_filters' => true
);
 $the_query = get_posts( $args ); ?>
<main class="blog-news">
	<div class="container-fluid fil-dariane scroll-fade-out-fast wide-picture">
	</div>
	<div class="container">
		<div class="row mb-200">
			<div class="col-12 pt-10 blog-title fast-opacity">
				<h1 class="fs-45 text-gold ubuntu text-left"> Blog</h1>
			</div>
		</div>
		<a class="f-s-12 ubuntu-light pull-right pr-15" href="<?php echo home_url(); ?>"><?php _e( 'Back to home page', 'starterTheme' ); ?></a>

		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		} ?>
	</div>
	<div class="container-fluid">
		<div class="container">
			<div class="row mt-10 row-articles">
				<?php
					/* Start the Loop */
						foreach ( $the_query as $post ) : setup_postdata( $post );
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
									<?php
										 $count_posts = wp_count_posts();
										 $published_posts = $count_posts->publish;
										 if ($published_posts > 3) {
										 	$taille_grille = 4;
										 } else {
										 	$taille_grille = 12;
										 }
									?>
									<div class="p-10 col-lg-<?php echo $taille_grille ?> col-md-<?php echo $taille_grille ?> col-xs-12">
									<a onclick="getArticle(<?= $post->ID ?>, displayArticle)">
										<article class="d-flex align-end anim-300 recent-post-nav p-0 mosaique" style="height:400px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url('<?php echo $thumb['0'];?>');">
											<div class="titre-content d-flex">
												<div class=" d-flex ml-10 p-0">
													<div class="">
														<div class="text-gold fs-18 titre-mosaique">
															<?php the_title('') ?>
														</div>
														<div class="cat-mosaique ubuntu fs-12">
														 <?php //echo get_the_category($post->ID)[0]->name ?>
														</div>
													</div>
												</div>
												<div class="col-1 p-0 d-flex ml-auto mr-15">
													<i class="fa fa-angle-right fs-30 text-gold" aria-hidden="true"></i>
												</div>
											</div>
										</article>
									</a>
									</div>
					<?php endforeach;?>
				<?php
				?>
			</div>
			<div class="row mt-80 mb-80">
				<!-- pagination -->
				<div class="mx-auto">
					<div class="blog-pagination text-gold">
						<?php html5wp_pagination(); ?>
					</div>
				</div>
				<!-- /pagination -->
			</div>
		</div>
	</div>
</main>
<div id="popup-article">
	<div class="container">
		
	</div>
</div>
<?php get_footer(); ?>

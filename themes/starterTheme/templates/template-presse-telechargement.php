<?php /* Template Name: Presse – Téléchargement */ get_header(); ?>

<main role="main" class="presse">
	<section class="container ">
		<div class="row justify-content-between">
			<div class="numero-page-un gothamb text-center text-gold-light col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
				<div class="nombre-page-chateau">
					<div class="trait-nombre apparition">
					</div>
					<p class="apparition">5</p>
					<div class="trait-nombre2 apparition">
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
				<div class="container-v-chateau pt-80 text-right apparition">
					<svg width="112px" height="155px" viewBox="0 0 112 155" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<title><?php _e( 'V Jean Voisin' ); ?></title>
						<defs></defs>
						<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.125874534">
							<g id="03-LE-CHATEAU-VOISIN" transform="translate(-1198.000000, -66.000000)" fill="#727272">
								<path d="M1254,174.70734 L1293.2294,66 L1310,66 L1254.76951,219.066405 L1254.02657,221 L1254,174.70734 Z M1254,174.70734 L1253.97343,221 L1253.23049,219.066405 L1198,66 L1214.7706,66 L1254,174.70734 Z" id="Mask"></path>
							</g>
						</g>
					</svg>
				</div>
				<div class="d-flex apparition text-right ">
					<h1 class="text-uppercase ml-auto pt-50"><span class="fs-50 titre-decouvrez gothamb text-grey-light"><?php _e( 'Téléchargement' ); ?></span><br><span class="fs-90 titre-decouvrez gothamb text-gold"><?php _e( 'Presse' ); ?></span></h1>
				</div>
			</div>
		</div>
	</section>
	<section class="container ">
		<div class="row">
			<div class="mt-80 mb-30 col">
				<h2 class="text-uppercase gothamb text-grey-light fs-26 apparition animation-fade-up">
					<?php _e( 'Nos actualités' ); ?>
				</h2>
			</div>
		</div>
		<div class="row  pb-100">
			<div class="col d-flex flex-column ">
				<?php
				if( have_rows('actu_table') ):
				    while ( have_rows('actu_table') ) : the_row();?>
						<div class="actu-table d-flex ">
							<div class="date-table pt-3 pr-3 pb-3 pl-0">
								<p class="text-gold m-0  fs-20"><?php the_sub_field('date');?></p>
							</div>
							<div class="title-actu p-3 m-0">
								<h2 class="m-0 fs-20"><?php the_sub_field('title');?></h2>
							</div>
							<div class="download ml-auto p-3">
								<a class="text-gold  fs-20" href="<?php the_sub_field('link');?>" download><?php _e( 'Télécharger' ); ?></a>
							</div>
						</div>
				    <?php endwhile;
				else :
				endif;
				?>
			</div>
		</div>
	</section>
	<section class="container-fluid bkg-black pb-100">
		<div class="row text-right flex pt-100 pb-100">
			<h3 class=" kit-presse fs-20 text-grey-light ml-auto mr-100 d-inline-block"><a download class="kit-presse p-3 border"href=""><?php _e( 'Téléchargez notre kit presse' ); ?></a></h3>
		</div>
		<div class="row pb-100">
			<div class="col-2 pt-100 text-right gothamb align-middle fs-18 text-white">
				<h2 class="bolder title-medias text-grey fs-190 text-uppercase"><?php _e( 'Médias' ); ?></h2>
			</div>
			<div class="col-10 pl-50 d-flex flex-wrap galerie">
				<?php $images = get_field('galerie'); if( $images ): ?> <!-- This is the gallery filed slug -->
					<?php foreach( $images as $image ): ?> <!-- This is your image loop -->
						<div class="col-4 p-0">
							<a rel='first' id="single_image" href="<?php echo $image['url']; ?>">
								<img class="w-100" src="<?php echo $image['sizes']['thumbnail-500']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>
					<?php endforeach; ?> <!-- This is where the image loop ends -->
				<?php endif; ?> <!-- This is where the gallery loop ends -->
			</div>
		</div>
	</section>
</main>
<?php get_footer('footer'); ?>

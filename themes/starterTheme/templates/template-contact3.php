<?php /* Template Name: Contact3 */ get_header(); ?>
<main role="main" class="main-content">
	<?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/title.php"); ?>
	<div class="container-fluid">
		<!-- section -->
		<section>
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="row justify-content-center" >
					<div class="col-12 p-0 contact3">
						<div id="map"></div>
					</div>
					<div class="col-5 col-xs-12 form-contact3">
						<h1 class="text-center mt-30 mb-30"><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<div id="rs">
						  <?php social_media('google'); social_media('twitter'); social_media('facebook'); ?>
						</div>
					</div>
				</div>
			</article>
			<!-- /article -->
		<?php endwhile; ?>

		<?php else: ?>
			<!-- article -->
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
			</article>
			<!-- /article -->
		<?php endif; ?>
		</section>
		<!-- /section -->
	    <script async defer
	      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYM2FZ6PBFvla3XFMkE6xALHBw2KPY3LY&callback=initMap">
	    </script>
	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>

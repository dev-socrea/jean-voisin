<?php /* Template Name: Contact */ get_header(); ?>
<main role="main" class="contact bkg-black">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section class="mb-100 container mb-250">
		<div class="row justify-content-between">
			<div class="numero-page-un gothamb text-center text-gold-light col-6">
				<div class="nombre-page-chateau">
					<div class="trait-nombre apparition">
					</div>
						<h1 class="apparition text-uppercase text-right ml-auto pt-50  mt-100"><span class="bolder text-uppercase titre-decouvrez gothamb text-black"><?php _e( 'Contact' ); ?></span></h1>
					<div class="trait-nombre2 apparition">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container pb-100">
		<div class="row ">
			<div class="apparition col p-0">
				<?php the_content('');?>
			</div>
			<div class="apparition col ls-3 text-white coordonnees">
				<h2 class="fs-16 text-gold"><strong><?php the_field('nom', 'option'); ?></strong></h2>
				<p><?php the_field('adresse', 'option'); ?></p>
				<div class="mb-30 mt-30">
					<a class="d-inline-block text-gold border border-gold p-3 fs-20"><?php the_field('lien_google_map', 'option'); ?>Google map</a>
				</div>
				<p class="d-inline-block clear"><strong><?php the_field('telephone', 'option'); ?></strong></p>
				<p class="ls-1"><?php the_field('fax', 'option'); ?></p>
			</div>

		</div>
	</section>
	<!-- /section -->
	 <!-- <div id="map"></div>
	    <script async defer
	    	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYM2FZ6PBFvla3XFMkE6xALHBw2KPY3LY&callback=initMap">
	    </script>
	</div> -->
	<?php endwhile; ?>

			<?php else: ?>
				<!-- article -->
				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
				</article>
				<!-- /article -->
			<?php endif; ?>
</main>

<!-- /container-fluid -->
<?php get_footer(); ?>

<?php
$tmp_name = tmp_name();
$query = new WP_Query(array('post_type' => 'first-custom-post'));
$terms = get_terms('custom-taxonomy-1');
$total = wp_count_posts('first-custom-post')->publish;
?>
<?php /* Template Name: Cover Flow */ get_header(); ?>

    <div class="container-fluid">
            <!-- categories -->
            <div class="col-lg-12 col-md-12 col-xs-12">
                <?php
                echo "<a href='#' class='term-filter' count-posts=$total><h4>"; _e('All', 'starterTheme'); echo "</h4></a>";
                foreach ($terms as $term){
                    $term_link = get_term_link($term);
                    echo "<a href='#' class='term-filter' count-posts=$term->count data-id=$term->term_id data-slug='".$term->slug."'><h4>".$term->name."</h4></a>";
                }
                ?>
            </div>
            <!-- /categories -->
            <main role="main" id="scroll-area" class="main-content">
                <!-- section -->
                <section class="items-container anim-500">
                    <div class="ajax">
                        <ul class="tax">
                            <?php if ($query->have_posts()): while ($query->have_posts()) : $query->the_post(); ?>
                                <?php get_template_part('loops/loop', 'cover-flow'); ?>
                            <?php endwhile; endif; ?>
                        </ul>
                        <div class="more"></div>
                        <!-- pagination -->
                        <div class="load-more">
                            <?php
                            echo "<button id='btn-load' count-posts=$total>";
                            _e('Load More', 'starterTheme');
                            echo "</button>";
                            ?>
                        </div>
                        <!-- /pagination -->
                    </div>
                </section>
                <!-- /section -->
            </main>
    </div>
    <!-- /container-fluid -->

    <script type="text/javascript">
        (function($){
            $(document).ready(function(){
                var termFilter = '.term-filter',
                    loadMore  = '.load-more button',
                    triggers  = [termFilter, loadMore];
                var postType  = 'first-custom-post',
                    taxSlug   = 'custom-taxonomy-1',
                    tmpName   = <?=json_encode($tmp_name)?>,
                    nbPosts, termSlug, termId, selector;

                for(var i=0; i<triggers.length;i++){

                    /* For each click on ajax triggers */
                    $(triggers[i]).on('click', function(){

                        /* Creating a $variable by getting values from tags' attributes */
                        termSlug = $(this).attr('data-slug') != undefined ? $(this).attr('data-slug') : 'null';
                        termId   = $(this).attr('data-id')   != undefined ? $(this).attr('data-id')   : 'null';
                        selector = $(this).attr('id')        != undefined ? $(this).attr('id')        : 'null';
                        nbPosts  = $(this).attr('count-posts');

                        /* Then, instantiate ajax function with values previously declared
                         {{get_template_directory_uri}}/assets/js/ajax.js */
                        ajaxSort(postType, taxSlug, nbPosts, termSlug, termId, tmpName, selector);
                    })
                }

                var zone = $('section.items-container');
                zone.resizr({
                    currentTotal : 2,
                    offsetVisible : 5,
                    ratioHover : 2
                });
            });
        })(jQuery, this);
    </script>

<?php get_footer(); ?>
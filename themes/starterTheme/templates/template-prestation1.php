<?php /* Template Name: Prestation1 */ get_header(); ?>
<main role="main" class="main-content">
	<?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/starterTheme/includes/title.php");?>
	<section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<div class="container"></div>
	<div class="container-fluid">
		<!-- <div class="row presta-title">
			<h1><?php the_title('')?></h1>
			<?php the_content('')?>
		</div> -->
			<?php
			if( have_rows('prestations') ):
				while ( have_rows('prestations') ) : the_row();?>

					<article class="animer row presta-1 align-items-center no-flex">
						<div class="col-6 col-xs-12 p-0 ">
							<img src="<?php the_sub_field('bloc_image'); ?>" alt="">
						</div>
						<div class="col-6 col-xs-12">
							<div class="text-container">
								<div class="my-auto">
								<?php the_sub_field('bloc_texte'); ?>
								</div>
							</div>
						</div>
				</article>
				 <?php   endwhile;
			else :
			endif;
			?>
		<?php endwhile; ?>
		<?php else: ?>
			<!--  -->
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
			</article>
		<?php endif; ?>

	</div>
	</section>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>

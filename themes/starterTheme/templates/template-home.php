<?php /* Template Name: Home */ get_header(); ?>
<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
<div class="img-background-home-page" style="background: linear-gradient(141.73deg, rgba(129,129,129,0.54) 0%, rgba(11,11,11,0.88) 100%), url(<?php echo $url; ?>)">
	<div class="menu-home-page d-flex">
		<div class="apparition menu-btn">
			<div class="menu-btn-home">
				<div class="barre-btn">
				</div>
			</div>
			<div class="text-menu-btn-home align-top">
				<?php _e( 'MENU' ); ?>
			</div>
		</div>
		<div class="apparition ml-auto">
			<?php wp_nav_menu( array( 'theme_location' => 'menu-home-page' ) ); ?>
		</div>
	</div>
	<div class="d-flex">
		<div class="ml-auto mt-80 mr-100 hover-anim-home">
			<div class="apparition content-logo-home-page d-flex justify-content-center">
				<img src="/jean-voisin/wp-content/themes/starterTheme/assets/img/logo-home-age-jean-voisin-white.svg" alt="Logo Jean Voisin">
			</div>
			<a href="<?php echo home_url('/le-chateau/'); ?>">
				<div class="apparition texte-sous-logo-home-page d-flex justify-content-center text-center text-white fs-18 gothamb mt-80">
					<?php the_field('texte_sous_logo') ?>
				</div>
				<div class="apparition picto-cadena d-flex justify-content-center no-lazy anim-300">
					<img src="/jean-voisin/wp-content/themes/starterTheme/assets/img/cadena_jean_voisin.svg" alt="Cadena Jean Voisin">
				</div>
			</a>
			<div class="apparition container-svg-home no-lazy">
				<img src="/jean-voisin/wp-content/themes/starterTheme/assets/img/logo-home.svg" alt="Chateau Jean Voisin">
			</div>
			<div class="text-uppercase text-white gothamb text-center fs-18 margin-neg-home-texte apparition">
				<?php _e ('Saint-émilion grand cru') ?>
			</div>
		</div>
	</div>
	<div class="apparition langue-home text-center text-white">
		FR <i class="fa fa-caret-up" aria-hidden="true"></i>
	</div>
</div>
<?php get_footer('footer'); ?>

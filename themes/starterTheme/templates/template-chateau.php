<?php /* Template Name: Le chateau */ get_header(); ?>
<div class="container-image apparition" style="background: url(<?php the_field('image_de_fond'); ?>)">
	<div class="container">
		<div class="row justify-content-between">
			<div class="numero-page-un gothamb text-center text-gold-light col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
				<div class="nombre-page-chateau">
					<div class="trait-nombre apparition">
					</div>
					<p class="apparition">1</p>
					<div class="trait-nombre2 apparition">
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
				<div class="container-v-chateau pt-80 text-right apparition">
					<svg width="112px" height="155px" viewBox="0 0 112 155" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<title>V Jean Voisin</title>
						<desc>Created with Sketch.</desc>
						<defs></defs>
						<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.125874534">
							<g id="03-LE-CHATEAU-VOISIN" transform="translate(-1198.000000, -66.000000)" fill="#727272">
								<path d="M1254,174.70734 L1293.2294,66 L1310,66 L1254.76951,219.066405 L1254.02657,221 L1254,174.70734 Z M1254,174.70734 L1253.97343,221 L1253.23049,219.066405 L1198,66 L1214.7706,66 L1254,174.70734 Z" id="Mask"></path>
							</g>
						</g>
					</svg>
				</div>
				<div class="d-flex apparition">
					<h1 class="text-uppercase ml-auto pt-50"><span class="fs-50 titre-decouvrez gothamb text-grey-light">Découvrez</span><br><span class="fs-90 titre-decouvrez gothamb text-gold"><?php the_title(); ?></span></h1>
				</div>
				<div class="description-chateau text-right mt-80 mb-80">
					<h2 class="text-uppercase gothamb text-grey-light fs-26 after-titre apparition"><?php the_field('titre_description') ?></h2>
					<div class="fs-18 gothamb apparition"><?php the_field('description') ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="img-background-deuxieme-partie" style="background: linear-gradient(0deg, rgba(57, 57, 57, 0.8) 0%, rgba(57, 57, 57, 0.8) 100%), url(<?php the_field('image_de_fond_deuxieme_partie'); ?>)">
	<div class="container">
		<div class="row pb-100">
			<div class="col pt-100 text-right gothamb align-middle fs-18 text-white">
				<?php the_field('texte_deuxieme_partie'); ?>
			</div>
			<div class="col pl-50">
				<div class="photo-neg-chateau parallax" style="background: url(<?php the_field('image_deuxieme_partie'); ?>)">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="emplacement" class="container-fluid pl-100 div-marge-neg-chateau">
	<h3 class="fs-70 gothamb text-uppercase titre-emplacement text-white pt-50 pb-100"><?php the_field('titre_troisieme_partie'); ?></h3>
	<div class="container-img-emplacement pb-100">
		<div class="w100 img-emplacement row" style="background: url(<?php the_field('photo_troisieme_partie'); ?>)">
		</div>
	</div>
	<div class="row">
		<div class="col text-right text-white gothamb ml-100">
			<?php the_field('texte_quatrieme_partie'); ?>
		</div>
		<div class="col">
			<div class="container-logo-chateau h-100 d-flex align-middle justify-content-center">
				<img src="/jean-voisin/wp-content/themes/starterTheme/assets/img/logo-home-age-jean-voisin-white.svg" alt="Logo Jean Voisin">
			</div>
		</div>
	</div>
</div>
<?php get_footer('footer'); ?>

<?php get_header();?>
<main class="blog-news">
	<div class="container-fluid fil-dariane scroll-fade-out-fast">
		<div class="row">
			<div class="mask-white-after col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-100 p-b-100 p-t-10 blog-title">
				<h1 class="f-s-50 text-white text-right p-t-80 schoko "><?php _e( 'Blog / Actualités', 'starterTheme' ); ?></h1>

				<a class="f-s-16 text-white pull-right p-r-15" href="<?php echo home_url(); ?>"><?php _e( 'Retour à la page d\'accueil', 'starterTheme' ); ?></a>

				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="container">
			<div class="row m-t-10 row-articles">
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 p-5 m-b-10">
				<?php
				if ( have_posts() ) :
					/* Start the Loop */
						while ( have_posts() ) : the_post();
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
								<a href="<?php echo get_permalink('') ?>">
									<?php
										$count_posts = wp_count_posts();
										$published_posts = $count_posts->publish;
										if ($published_posts > 3) {
											$taille_grille = 4;
										} else {
											$taille_grille = 12;
										}
									?>
									<div class="p-l-10 p-r-10 p-b-10 p-t-10 col-lg-<?php echo $taille_grille ?> col-md-<?php echo $taille_grille ?> col-xs-12">
										<article class="flex align-end anim-300 recent-post-nav no-padding mosaique" style="height:400px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url('<?php echo $thumb['0'];?>');">
											<div class="titre-content flex">
												<div class=" flex p-0">
													<div class="">
														<div class="text-gold f-s-18 titre-mosaique">
															<?php the_title('') ?>
														</div>
														<div class="cat-mosaique f-s-12">
															<?php echo get_the_category()[0] -> name; ?>
														</div>
													</div>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 p-0 flex m-l-auto m-r-15">
													<i class="fa fa-angle-right f-s-30 text-gold" aria-hidden="true"></i>
												</div>
											</div>
										</article>
									</div>
								</a>
					<?php endwhile;?>
				<?php endif;
				?>
			</div>
			</div>
			<div class="row m-t-80 m-b-80">
				<!-- pagination -->
				<div class="text-center">
					<div class="blog-pagination text-gold">
						<?php html5wp_pagination(); ?>
					</div>
				</div>
				<!-- /pagination -->
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>

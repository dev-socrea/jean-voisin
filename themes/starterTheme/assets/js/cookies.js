function createCookie(name, value, days){
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expire = "; expires=" + date.toGMTString();
    }
    else var expire = "";
    document.cookie = name + "=" + value + expire + "; path=/";
}

function getCookieVal(offset){
    var endstr = document.cookie.indexOf (";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return decodeURI(document.cookie.substring(offset, endstr));
}

function readCookie(name){
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal (j);
        i = document.cookie.indexOf(" ",i) + 1;
        if (i == 0) break; }
    return null;
}

function checkCookie(){
    var popIn = document.getElementById("checkBrowser");
    if(readCookie("cookieMajBrowserStarterTheme") != null){
        popIn.className += " browserOk";
    }
}

function crossOut(){
    var popIn = document.getElementById("checkBrowser");
    popIn.style.bottom = "-150px";
    setTimeout(function(){ popIn.style.display = "none" }, 500);
    if(readCookie("cookieMajBrowserStarterTheme") == null){
        createCookie("cookieMajBrowserStarterTheme", "ok", 1);
    }
}

function cookieOk(){
    var popIn = document.getElementById("cookie");
    popIn.style.bottom = "-150px";
    setTimeout(function(){ popIn.style.display = "none" }, 500);
    if(readCookie("cookieOk") == null){
        createCookie("cookieOk", "ok", 20);
    }
}
function verifCookie(){
    var popIn = document.getElementById("cookie");
    if(readCookie("cookieOk") != null){
        popIn.style.display = "none";
    }else{
        popIn.style.display = "block";
    }
}

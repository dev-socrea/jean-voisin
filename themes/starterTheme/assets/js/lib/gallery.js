
	/* Galerie 1 */
	var $c = 1;
	$(".galerie_container div").addClass( "col-4 no-padding" );

	var width1 = jQuery(".galerie_container div:nth-of-type(1)").width();
	$(".galerie_container div:nth-of-type(1)").css( "height", width1 );
	var marginType = jQuery(".galerie_container div:nth-of-type(1)").height();


	function tuile_2p_1g() {
	  for (var i = 1; i <= 3; i++) {
			jQuery(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
				if (i == 1) {
					//...
				} else if (i == 2) {
					$(this).removeClass( "col-4" );
					$(this).addClass( "col-8" );
				} else if (i == 3) {
					$(this).css({ marginTop: -marginType });
				}
			})
			$c = $c + 1;
		}
	}

	function tuile_2p_1v() {
	  for (var i = 1; i <= 2; i++) {
			jQuery(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
				if (i == 1) {
					//...
				} else if (i == 2) {
	        $(this).css({ marginRight: marginType});
				} else if (i == 3) {
					//...
				}
			})
			$c = $c + 1;
		}
	}

	function tuile_1g_1p_1v() {
	  for (var i = 1; i <= 2; i++) {
			jQuery(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
				if (i == 1) {
	        $(this).removeClass( "col-4" );
					$(this).addClass( "col-8" );
					$(this).css({ height: marginType*2 });
				} else if (i == 2) {
	        $(this).css({ marginBottom: marginType });
				} else if (i == 3) {
					//...
				}
			})
			$c = $c + 1;
		}
	}

	function tuile_1v_2p() {
	  for (var i = 1; i <= 2; i++) {
			jQuery(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
				if (i == 1) {
	        $(this).css({ marginLeft: marginType });
				} else if (i == 2) {
	        //...
				} else if (i == 3) {
					//...
				}
			})
			$c = $c + 1;
		}
	}

	function tuile_1p_1v_1p() {
	  for (var i = 1; i <= 2; i++) {
			jQuery(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
				if (i == 1) {
	        //...
				} else if (i == 2) {
	        $(this).css({ marginLeft: marginType });
				} else if (i == 3) {
					//...
				}
			})
			$c = $c + 1;
		}
	}


	/* Fancy Box zoom */
	$(document).ready(function() {

		$("a#single_image").fancybox({
			beforeShow: function () {
		        this.title = $(this.element).find("img").attr("title");
		    }
		});

		/* Using custom settings */
		$("a#inline").fancybox({
			'hideOnContentClick': true
		});
		/* Apply fancybox to multiple items */
		$("a.group").fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600,
			'speedOut'		:	200,
			'overlayShow'	:	false
		});
	});


	/* Galerie 2 */

	var $c = 1;
	// $(".galerie_container div").addClass( "col-4 no-padding" ); // on ajoute une classe de référence aux div du conteneur

	var width1 = jQuery(".galerie_container div:nth-of-type(1)").width(); // variable contenant la largeur de la première div
	$(".galerie_container div:nth-of-type(1)").css( "height", width1 );   // on ajoute la variable précédente en hauteur à div du conteneur
	var height1 = jQuery(".galerie_container div:nth-of-type(1)").height();


	//console.log(width1);
	//console.log(height1);

	function col4_col8(){
		  for (var i = 1; i <= 2; i++) {
				$(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
					if (i == 1) {
						$(this).addClass( "col-4" );
						//...
					} else if (i == 2) {
						$(this).removeClass( "col-4" );
						$(this).addClass( "col-8" );
						$(this).css({height: height1});
					}
				})
				$c = $c + 1;
			}
		}


	function col8_col4() {
	  for (var i = 1; i <= 2; i++) {
				$(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
					if (i == 1) {
						$(this).removeClass('col-4');
						$(this).addClass('col-8');
						$(this).css({height: height1});

					} else if (i == 2) {
						//...
						$(this).css({height: height1});
				}
			})
			$c = $c + 1;

		}
	}
	function col4_col4_col_4() {
	  for (var i = 1; i <= 3; i++) {
			jQuery(".galerie_container div:nth-of-type(" + $c + ")").each(function() {
				if (i == 1) {
					$(this).removeClass( "col-8" );
					$(this).addClass( "col-4" );
					//$(this).css({ height: marginType*2 });
				} else if (i == 2) {
					//...
				}
			})
			$c = $c + 1;

		}
	}

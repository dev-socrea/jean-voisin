<!-- Fermeture de la balise section -->  
</section>
 
<footer class="relative full-width bg-dark gray">  
<div class="row">

    <div id="top-footer" class="full-width margin-bottom border-bottom">
    
        <ul class="rs reset right">
            <li class="right"><a href="<?php echo stripslashes( get_option('facebook', '')); ?>" class="reset block left picto gray"><i class="fa fa-facebook"></i></a></li>
            <li class="right"><a href="<?php echo stripslashes( get_option('twitter', '')); ?>" class="reset block left picto gray"><i class="fa fa-youtube"></i></a></li>
            <li class="right"><a href="<?php echo stripslashes( get_option('google', '')); ?>" class="reset block left picto gray"><i class="fa fa-google"></i></a></li>
        </ul>
        
       <!-- <?php /* echo do_shortcode('[mc4wp_form]'); */?> -->
        
        <span class="size-21 white left"><?php echo stripslashes( get_option('phone', '')); ?></span>
    
    </div>
    
    <div id="content-footer" class="full-width">
    
        <div class="right text-right">
        <?php wp_nav_menu( array('menu' => '3', 'menu_class'=> 'menu reset')); ?>
        </div>
        
        <div class="left text-left espace">
            <h4>Make My Van Nantes</h4>
            <address>
            <?php echo stripslashes( get_option('rue', '')); ?> <br />
            <?php echo stripslashes( get_option('rue_bis', '')); ?> <br />
            <?php echo stripslashes( get_option('code-postal', '')); ?> <?php echo stripslashes( get_option('ville', '')); ?>
            </address>           
            <p><?php echo stripslashes( get_option('horaires', '')); ?></p>                 
        </div>


        <div class="left text-left">
            <h4>Make My Van Bordeaux</h4>
            <address>
            <?php echo stripslashes( get_option('rue_bordeaux', '')); ?> <br />
            <?php echo stripslashes( get_option('rue_bis_bordeaux', '')); ?> <br />
            <?php echo stripslashes( get_option('code-postal_bordeaux', '')); ?> <?php echo stripslashes( get_option('ville_bordeaux', '')); ?>
            </address>
            <p><?php echo stripslashes( get_option('horaires_bordeaux', '')); ?></p>                 
        </div>
    
    </div>
    
    <div role="contentinfo" id="copyright" class="full-width border-top text-center">
        <small>Copyright © <?php echo get_the_date('Y'); ?> -  <?php bloginfo( 'name' ); ?> Tous droits réservés - <a href="<?php echo site_url('/mentions-legales' ); ?>" class="white">Mentions légales</a> - Création <a href="http://www.b2w.fr/" class="white">b2w </a> & <a class="white" target="_blank" href="https://socreativ.com"> Sõcreativ</a></small>
    </div>

</div>     

</footer>                   
  
<?php wp_footer(); ?> 

<script>

printLink();

    // init lazy sur les images
    jQuery("img").each(function() {
      jQuery(this).addClass('lazy');
      var url = jQuery(this).attr("src");
      jQuery(this).removeAttr("src");
      jQuery(this).removeAttr("srcset");
      jQuery(this).attr("data-original", url);
    });

    // ne pas appliquer sur no-lazy
    jQuery(".no-lazy img").each(function() {
      console.log(jQuery(this).attr("data-original"));
      var url2 = jQuery(this).attr("data-original");
      jQuery(this).attr("src", url2);
      jQuery(this).removeClass("lazy");
    });

    jQuery(function(){
      jQuery("img.lazy").lazyload({
        effect : "fadeIn",
        threshold : 300 // PIXELS A PARTIR DESQUELS L'IMAGE SE CHARGE
      });
    });

$( document ).ready(function() {
	cookieChoices.showCookieConsentBar('Ce site utilise des cookies pour améliorer la navigation et adapter le contenu en mesurant le nombre de visites et de pages vues.', 'OK', 'En savoir plus', 'http://makemyvan.fr/cookies');

});

jQuery(function(){   
jQuery(".eachpost").mouseover(function(){jQuery(this).css("cursor","pointer").find("a").css("text-decoration","none");}).mouseout(function(){jQuery(this).find("a").css("text-decoration","none");}).click(function(e){document.location.href = jQuery(this).find("a").attr("href");e.preventDefault();});       
});

jQuery(document).ready(function(){   
    jQuery( "div.accordion" ).accordion({     
    collapsible: true        
    });   
    jQuery('li.menu-item-has-children ul').on('mouseover',function(){
       jQuery(this).parent().addClass('active');
    }).on('mouseout',function(){
       jQuery(this).parent().removeClass('active');    
    });
    jQuery( "#content-actualite" ).each(function( index ) {
      jQuery( this ).find( "article:nth-child(2)" ).addClass( "first" );
    }); 
    jQuery( "#contenu" ).each(function( index ) {
      jQuery( this ).find( ".post:nth-child(3n+1)" ).addClass( "last" );
    }); 
    
      
    // TUBULAR : Configuration de la background vidéo
        // MAJ Aout 2015 - annuler la video pour la remplacer par un slider
        //jQuery('#wrapper').tubular({
        //  videoId: <?php echo "'".get_option('id_video', '')."'"; ?>,
        //  wrapperZIndex: 5
        //});    
        
    // Adapter la taille d'une div à celle du navigateur
    // MAJ Aout 2015 - Annuler le full-screen
        //var Sy = jQuery(window).height();// hauteur de l'écran
        //var Sx = jQuery(window).width();// largeur de l'écran
        //    jQuery('#video').css('height',Sy+'px');
        //    jQuery('#video').css('width',Sx+'px');  
        //jQuery( window ).resize(function() {
        //    jQuery("#video").css( "height", jQuery( window ).height() );
        //});
        
    // OWL Carousel : Configuration des sliders et carousel
    jQuery(document).ready(function(){
        jQuery(".owl-caroussel").owlCarousel({
            loop:true,
            autoplay:true,
            autoplayTimeout:8000,
            autoHeight:true,
            items:1, 
            nav:true,
            navText:["<span class='picto picto-arrows-left'></span>","<span class='picto picto-arrows-right'></span>"],
            responsive: true,
            itemsDesktop : [1040,1],
            itemsTablet: [1040,1],
            itemsMobile : [479,1]
        });
        jQuery(".owl-slider").owlCarousel({
            loop:true,
            autoPlay:5000,
            items:1,
            pagination: false,
            navigation:true,
            navigationText:["<span class='picto picto-arrows-left white'></span>","<span class='picto picto-arrows-right white'></span>"],
            responsive: true,
            autoHeight: true, 
            itemsDesktop : [1040,1],
            itemsTablet: [1040,1],
            itemsMobile : [479,1]
        });
        jQuery(".owl-realisation").owlCarousel({
            loop:true,
            autoPlay:3000,
            items:4,
            pagination: false,
            navigation:true,
            navigationText:["<span class='picto picto-arrows-left white'></span>","<span class='picto picto-arrows-right white'></span>"],
            responsive: true,
            itemsDesktop : [1040,4],
            itemsTablet: [1040,3],
            itemsMobile : [479,2]
        });
    });
    // BIGSLIDE : Configuration du menu responsive
    jQuery(document).ready(function() {
        jQuery('.menu-link').bigSlide({
          'push': ('.push')
        });
    });            
    // MASONRY : Affichage pour les articles du blog
    jQuery('#list-post').masonry({
      singleMode: true,
      itemSelector: '.post'
    });
    
});

jQuery(window).load(function() {

    jQuery('#list-post').masonry({
      singleMode: true,
      itemSelector: '.post'
    });
    
});

function height(bloc){

    var hauteur;

    

    if( typeof( window.innerWidth ) == 'number' )

        hauteur = window.innerHeight;

    else if( document.documentElement && document.documentElement.clientHeight )

        hauteur = document.documentElement.clientHeight;

    

    document.getElementById(bloc).style.height = hauteur+"px";

}


window.onload = function()
{ 
    if ($(window).width() > 1024)
        height("slider_size");
    else 
        $("#slider_size").css("height", "0px");

};

window.onresize = function()
{ 
    if ($(window).width() > 1024)
        height("slider_size");
    else 
    
        $("#slider_size").css("height", "0px");

};

/*var largeur = document.documentElement.clientWidth;
 if(largeur<400) {
var source = document.getElementById('slider_size');
source.style.visibility='hidden';
} */

</script>

</body>
</html>
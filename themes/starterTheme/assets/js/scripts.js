(function ($, root, undefined) {

	$(function () {

        $(document).ready(function(){
			// EFFET PARALLAX
			$(".parallax").each(function() {
				var offsetParallax = $(this).offset();
				var heightParallax = $(this).height();
				var scrollWindow = $(window).scrollTop();
				effect = (offsetParallax.top - scrollWindow - heightParallax)/8;
				$(this).css('transform', 'translateY('+ effect + 'px)');
			});
			$(window).scroll(function() {
				$(".parallax").each(function() {
					var offsetParallax = $(this).offset();
					var heightParallax = $(this).height();
					var scrollWindow = $(window).scrollTop();
					effect = (offsetParallax.top - scrollWindow - heightParallax)/8;
					$(this).css('transform', 'translateY('+ effect + 'px)');
					// console.log(offsetParallax.top + " " + scrollWindow + " " + heightParallax);
				});
			});


			// //CLICK menu
			// $( ".menu-btn" ).click(function() {
			// 	$("body").addClass("menu-open");
			// 	$(".container-border-menu-open").fadeIn(300);
			// 	$(".container-menu-vertical").fadeOut(300);
			// 	$(".menu-vertical").fadeOut(300);
			//
			// });
			// $( ".close-menu-open" ).click(function() {
			// 	$("body").removeClass("menu-open");
			// 	$(".container-border-menu-open").fadeOut(300);
			// 	$(".container-menu-vertical").fadeIn(300);
			// 	$(".menu-vertical").fadeIn(300);
			// });


			// APPRARITION
			var delay = 0;
			 $('.apparition').each(function() {
			   var $li = $(this);
			   setTimeout(function() {
				 $li.addClass('animation-fade-up');
			   }, delay+=100); // delay 100 ms
			 });

			//  TITRE FANCYBOX
			 $(document).click(function() {
 				setTimeout(function() {
 					var url = $(".fancybox-image").attr('src');
 					var title_img = $(".fancybox-image").attr('alt');
 					$( ".fancybox-skin" ).append( '<div class="row p-l-10 p-r-10"><div class="download f-s-18 p-10 col-lg-offset-4 text-right"><a href="' + url + '"download><i class="fa fa-download" aria-hidden="true"></i> Download</a></div></div>');
 				}, 1000);
 				setTimeout(function() {
 					$( ".download" ).remove();
 				}, 800);
 			});

			jQuery(".top-page").fadeOut();

            'use strict';
						$("ul,li,a").addClass("anim-300");
                        $( "ul.sub-menu").removeClass("anim-300");



                /* DOM Dimensions */

				$(window).on("load resize", function () {

					// Calculs Body et menu
					var W_Logo_Menu = $('.logo-menu-vertical').outerWidth(),
						W_Window = $(window).width(),
						W_Calcul_Body = W_Window - W_Logo_Menu;
					$('.menu-vertical').width(W_Logo_Menu);
					$('body').width(W_Calcul_Body);
					$('.langue-menu').width(W_Logo_Menu);

                    var DOM = {
                        W: $(window).width(),
                        H: $(window).height(),
                        dimensions: function(){
                            console.clear();
                        }
                    };

                    //DOM.dimensions();
                    var header_top_height = $('#header-sticky').outerHeight();
                    $(".sticky-spacing").css('height', header_top_height)
                    ////// SETTING STICKY SCROLL ////////
                    var lastScrollTop = 0;
                    $(window).scroll(function(event){
                        var st = $(this).scrollTop();
                        var header_sticky = 200;
                        if (st > lastScrollTop){
                             if (st >= header_sticky) {
                                $("body").addClass("sticky");
                            }
                        } else {
                            if (st <= header_sticky) {
                                $("body").removeClass("sticky");
                            }
                        }
                       lastScrollTop = st;
                    });

	                 /*  CONTAINER FULL WIDTH TEXT GRID */
	    			 var w_container = $('.container').width();
	    			 var w_window =$( window ).width();
	                 $('.text-container').css("width", w_container/2);
	    			 $('.text-container-left-6').css("width", w_container/2);
	    			 $('.text-container-left-6').css("margin-left",(w_window - w_container)/2);

	                 $('.text-container-5').css("width", w_container*0.416666666666666);
	                 $('.text-container-5').css("margin-left", w_container*0.08333333333333);
	                 $('.text-container-left-5').css("width", w_container*0.416666666666666);
	                 $('.text-container-left-5').css("margin-left",(w_window - w_container)/2);
	                 $('.text-container-left-5').css("margin-right", w_container*0.08333333333333);

	                 $('.text-container-4').css("width", w_container*0.33333333333333333333333);
	                 $('.text-container-left-4').css("width", w_container*0.33333333333333333333333);
	                 $('.text-container-left-4').css("margin-left",(w_window - w_container)/2);
	                 $('.text-container-left-4').css("margin-right", w_container*0.16666666666666);

	                 $('.text-container8').css('width', w_container/1.5);
	                 $('.text-container8-left').css("width", w_container/1.5);
	                 $('.text-container8-left').css("margin-left",(w_window - w_container)/2);


        			 $('.text-container-left').css("width", w_container/2);
        			 $('.text-container-left').css("margin-left",(w_window - w_container)/2);

        			 var wf_container = $('.container-fluid').width();
                     $('.text-container-fluid').css("width", wf_container/2);

                }).resize();
                $('#page-loader').fadeOut(300);

            });

                /*SUB MENU HEADER LEFT */
                $( ".sub-menu" ).wrap( "<div class='sub-menu-top-position anim-300'></div>" );
                $('.main-navigation-zone > div > ul > li, .nav-mobile > ul > li').on("click",function(){
                    var containerWidth = $(".container.menu-container").outerWidth();
                    $('.sub-menu').css("width", containerWidth);
                    $(this).find(".sub-menu").slideToggle();
                    $(this).siblings().find(".sub-menu").fadeOut(300);

                    if($(".sub-menu:visible").length === 0 ){
                      $("#menu-overlay").fadeOut(300);
                    }else {
                      $("#menu-overlay").fadeIn(300);
                    }
                });


                var Wwidth = $(window).width();
                var containerWidth = $(".container.menu-container").outerWidth();
                $('.sub-menu').css("margin", "auto");
                $("#menu-overlay").on("click",function(){
                    $(".sub-menu").fadeOut(300);
                    $(this).fadeOut(300);
                });
             /* Modale */
             $( ".modale-parent" ).each(function() {
                 var last = $(this);
                 $(this).find('.modale-button').click(function() {
                     $('body').addClass("modale-open");
                     last.find('.modale').fadeIn("300");
                     last.find('.modale-content').addClass("animation-fade-up");
                     var coTop = last.find('.modale-content').outerHeight();
                     var coTmp = "calc(50% - " + coTop/2 + "px)";
                     last.find('.modale-content').css("top", coTmp);
                 });
                 $(this).find('.remove-modale').click(function() {
                     $('body').removeClass("modale-open");
                     $(this).parent().parent().parent().fadeOut("300");
                     $(this).parent().parent().parent().children('.modale-content').removeClass("animation-fade-up");
                 });
             });
             // /* Smooth Scroll */

             var $root = $("html, body");
             $("a").click(function() {
                 var href = $.attr(this, "href");
                 $root.animate({
                     scrollTop: $(href).offset().top
                 }, 500, function () {
                     window.location.hash = href;
                 });
                 return false;
             });

             outdatedBrowser({
                    lowerThan: 'transform',
                    languagePath: '../languages/outdatedbrowser/fr.html'
             })

             /** MODALE MENU **/

    });

})(jQuery, this);




/*!--------------------------------------------------------------------
JAVASCRIPT "Outdated Browser"
Version:    1.1.2 - 2015
author:     Burocratik
website:    http://www.burocratik.com
* @preserve
-----------------------------------------------------------------------*/

var outdatedBrowser = function(options) {

    //Variable definition (before ajax)
    var outdated = document.getElementById("outdated");

    // Default settings
    this.defaultOpts = {
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: '../languages/outdatedbrowser/fr.html'
    }

    if (options) {
        //assign css3 property to IE browser version
        if (options.lowerThan == 'IE8' || options.lowerThan == 'borderSpacing') {
            options.lowerThan = 'borderSpacing';
        } else if (options.lowerThan == 'IE9' || options.lowerThan == 'boxShadow') {
            options.lowerThan = 'boxShadow';
        } else if (options.lowerThan == 'IE10' || options.lowerThan == 'transform' || options.lowerThan == '' || typeof options.lowerThan === "undefined") {
            options.lowerThan = 'transform';
        } else if (options.lowerThan == 'IE11' || options.lowerThan == 'borderImage') {
            options.lowerThan = 'borderImage';
        }
        //all properties
        this.defaultOpts.bgColor = options.bgColor;
        this.defaultOpts.color = options.color;
        this.defaultOpts.lowerThan = options.lowerThan;
        this.defaultOpts.languagePath = options.languagePath;

        bkgColor = this.defaultOpts.bgColor;
        txtColor = this.defaultOpts.color;
        cssProp = this.defaultOpts.lowerThan;
        languagePath = this.defaultOpts.languagePath;
    } else {
        bkgColor = this.defaultOpts.bgColor;
        txtColor = this.defaultOpts.color;
        cssProp = this.defaultOpts.lowerThan;
        languagePath = this.defaultOpts.languagePath;
    } //end if options


    //Define opacity and fadeIn/fadeOut functions
    var done = true;

    function function_opacity(opacity_value) {
        outdated.style.opacity = opacity_value / 100;
        outdated.style.filter = 'alpha(opacity=' + opacity_value + ')';
    }

    // function function_fade_out(opacity_value) {
    //     function_opacity(opacity_value);
    //     if (opacity_value == 1) {
    //         outdated.style.display = 'none';
    //         done = true;
    //     }
    // }

    function function_fade_in(opacity_value) {
        function_opacity(opacity_value);
        if (opacity_value == 1) {
            outdated.style.display = 'block';
        }
        if (opacity_value == 100) {
            done = true;
        }
    }

    //check if element has a particular class
    // function hasClass(element, cls) {
    //     return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    // }

    var supports = ( function() {
        var div = document.createElement('div');
        var vendors = 'Khtml Ms O Moz Webkit'.split(' ');
        var len = vendors.length;

        return function(prop) {
            if (prop in div.style) return true;

            prop = prop.replace(/^[a-z]/, function(val) {
                return val.toUpperCase();
            });

            while (len--) {
                if (vendors[len] + prop in div.style) {
                    return true;
                }
            }
            return false;
        };
    } )();

    //if browser does not supports css3 property (transform=default), if does > exit all this
    if (!supports('' + cssProp + '')) {
        if (done && outdated.style.opacity !== '1') {
            done = false;
            for (var i = 1; i <= 100; i++) {
                setTimeout(( function(x) {
                    return function() {
                        function_fade_in(x);
                    };
                } )(i), i * 8);
            }
        }
    } else {
        return;
    } //end if

    //Check AJAX Options: if languagePath == '' > use no Ajax way, html is needed inside <div id="outdated">
    if (languagePath === ' ' || languagePath.length == 0) {
        startStylesAndEvents();
    } else {
        grabFile(languagePath);
    }

    //events and colors
    function startStylesAndEvents() {
        var btnClose = document.getElementById("btnCloseUpdateBrowser");
        var btnUpdate = document.getElementById("btnUpdateBrowser");

        //check settings attributes
        outdated.style.backgroundColor = bkgColor;
        //way too hard to put !important on IE6
        outdated.style.color = txtColor;
        outdated.children[0].style.color = txtColor;
        outdated.children[1].style.color = txtColor;

        //check settings attributes
        btnUpdate.style.color = txtColor;
        // btnUpdate.style.borderColor = txtColor;
        if (btnUpdate.style.borderColor) {
            btnUpdate.style.borderColor = txtColor;
        }
        btnClose.style.color = txtColor;

        //close button
        btnClose.onmousedown = function() {
            outdated.style.display = 'none';
            return false;
        };

        //Override the update button color to match the background color
        btnUpdate.onmouseover = function() {
            this.style.color = bkgColor;
            this.style.backgroundColor = txtColor;
        };
        btnUpdate.onmouseout = function() {
            this.style.color = txtColor;
            this.style.backgroundColor = bkgColor;
        };
    } //end styles and events


    // IF AJAX with request ERROR > insert french default
    var ajaxEnglishDefault = '<h6>Votre navigateur est obsolète!</h6>'
        + '<p>Mettez à jour votre navigateur pour afficher correctement ce site Web. <a href="http://outdatedbrowser.com/" ><div class="maj">Mettre à jour maintenant </div></a></p>'
        + '<p class="last"><a id="btnCloseUpdateBrowser"><div class="closediv">X</div></a></p>';

    //** AJAX FUNCTIONS - Bulletproof Ajax by Jeremy Keith **
    function getHTTPObject() {
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch ( e ) {
                try {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                } catch ( e ) {
                    xhr = false;
                }
            }
        }
        return xhr;
    }//end function

    function grabFile(file) {
        var request = getHTTPObject();
        if (request) {
            request.onreadystatechange = function() {
                displayResponse(request);
            };
            request.open("GET", file, true);
            request.send(null);
        }
        return false;
    } //end grabFile

    function displayResponse(request) {
        var insertContentHere = document.getElementById("outdated");
        if (request.readyState == 4) {
            if (request.status == 200 || request.status == 304) {
                insertContentHere.innerHTML = request.responseText;
            } else {
                insertContentHere.innerHTML = ajaxEnglishDefault;
            }
            startStylesAndEvents();
        }
        return false;
    }//end displayResponse

////////END of outdatedBrowser function
};


// BOUTON POUR REVENI EN HAUT DE LA PAGE
jQuery(document).scroll(function($) {
	var scrolltop = (jQuery(window).scrollTop());
	if (scrolltop > 300) {
		jQuery(".top-page").fadeIn(300);
	} else {
		jQuery(".top-page").fadeOut(300);
	}
});


//add animation class

function addAnimationByClass(classe){

	$(document).ready(function(){
		var elements = (jQuery(classe));
		elements.each(function(){
		if(testHauteur(this)){
		jQuery(this).css({opacity: 0});
	}
	});
	});

$(document).scroll(function($){

	var elements = (jQuery(classe));
	elements.each(function(){

		var scrollTop = (jQuery(window).scrollTop());

	var elementTop = jQuery(this).offset().top;
	var active = false;
	var distance = elementTop - scrollTop;


 if(distance < 400 && !active && testHauteur(this)){
	 jQuery(this).css({opacity: 1});
	 jQuery(this).addClass('animation-class');
	 active = true;
 }
 });
 });
}


function testHauteur(element){
	if(jQuery(element).offset().top < 100){
		return false;
	}else{
		return true;
	}
}




addAnimationByClass(".animer");


jQuery(document).ready(function() {

	scroll = $( window ).scrollTop();
	op = 0 + ((scroll) / 300);
	opfast = 0 + ((scroll) / 150);
	opinverse = 1 - ((scroll) / 100);

	var img = window.location.protocol + "//" + window.location.host + '/wp-content/themes/starterTheme/assets/img/single-default.jpg';
	 $('.scroll-fade-out').css({
    'background-image': '-webkit-gradient(linear, left top, left bottom, color-stop(80%, rgba(0,0,0,0)), color-stop(100%, rgba(0,0,0,' + op + '))), url(' + img + ')',
    'background-image': '-webkit-linear-gradient(top,       rgba(28,28,37,0) 0%, rgba(28,28,37,' + op + ') 70%), url(' + img + ')',
    'background-image': '   -moz-linear-gradient(top,       rgba(28,28,37,0) 0%, rgba(28,28,37,' + op + ') 70%), url(' + img + ')',
    'background-image': '     -o-linear-gradient(top,       rgba(28,28,37,0) 0%, rgba(28,28,37,' + op + ') 70%), url(' + img + ')',
    'background-image': '        linear-gradient(to bottom, rgba(28,28,37,0) 0%, rgba(28,28,37,' + op + ') 70%), url(' + img + ')',
	})

	$('.scroll-fade-out-fast').css({
   'background-image': '-webkit-gradient(linear, left top, left bottom, color-stop(80%, rgba(0,0,0,0)), color-stop(100%, rgba(0,0,0,' + op + '))), url(' + img + ')',
   'background-image': '-webkit-linear-gradient(top,       rgba(28,28,37,0) 0%, rgba(28,28,37,' + opfast + ') 50%), url(' + img + ')',
   'background-image': '   -moz-linear-gradient(top,       rgba(28,28,37,0) 0%, rgba(28,28,37,' + opfast + ') 50%), url(' + img + ')',
   'background-image': '     -o-linear-gradient(top,       rgba(28,28,37,0) 0%, rgba(28,28,37,' + opfast + ') 50%), url(' + img + ')',
   'background-image': '        linear-gradient(to bottom, rgba(28,28,37,0) 0%, rgba(28,28,37,' + opfast + ') 50%), url(' + img + ')',
   })

   $('.fast-opacity').css("opacity",opinverse);



});


jQuery(document).scroll(function() {

	scroll = $( window ).scrollTop();
	op = 0 + ((scroll) / 300);
	opfast = 0 + ((scroll) / 150);
	opinverse = 1 - ((scroll) / 100);

	var img = window.location.protocol + "//" + window.location.host + '/wp-content/themes/starterTheme/assets/img/single-default.jpg';
	 $('.scroll-fade-out').css({
    'background-image': '-webkit-gradient(linear, left top, left bottom, color-stop(80%, rgba(0,0,0,0)), color-stop(100%, rgba(0,0,0,' + op + '))), url(' + img + ')',
    'background-image': '-webkit-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,' + op + ') 70%), url(' + img + ')',
    'background-image': '   -moz-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,' + op + ') 70%), url(' + img + ')',
    'background-image': '     -o-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,' + op + ') 70%), url(' + img + ')',
    'background-image': '        linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,' + op + ') 70%), url(' + img + ')',
	})

	$('.scroll-fade-out-fast').css({
   'background-image': '-webkit-gradient(linear, left top, left bottom, color-stop(80%, rgba(0,0,0,0)), color-stop(100%, rgba(0,0,0,' + op + '))), url(' + img + ')',
   'background-image': '-webkit-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,' + opfast + ') 50%), url(' + img + ')',
   'background-image': '   -moz-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,' + opfast + ') 50%), url(' + img + ')',
   'background-image': '     -o-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,' + opfast + ') 50%), url(' + img + ')',
   'background-image': '        linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,' + opfast + ') 50%), url(' + img + ')',
   })

   $('.fast-opacity').css("opacity",opinverse);



});

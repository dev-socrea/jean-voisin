/* Prevent undefined functions using jQuery no conflict mode */
var $ = jQuery.noConflict();

/* Variable declaration */
var offset = 2,
    btn = $('.load-more button'),
    zone = $('section.items-container');

/* Ready to use functions */
function enableBtn() {
    /* Enable Button */
    btn.prop('disabled', false);
    /* Show Button */
    //btn.fadeIn();
}
function disableBtn() {
    /* Disable Button */
    //btn.prop('disabled', true);
    /* Hide Button */
    $('.load-more > button').fadeOut();
}
function resizrPlugin(){
    if(typeof zone.resizr != 'undefined'){
        zone.resizr({
            currentTotal: offset,
            offsetVisible: 5,
            ratioHover: 2
        });
    }
}
function clear() {
    offset = 2;
    resizrPlugin();
    enableBtn();
}

/* Core */
function ajaxSort(postType, taxSlug, nbPosts, termSlug, termId, tmpName, selector) {
    /* Local function */
    function deactivate(){if(offset >= nbPosts){offset = nbPosts; disableBtn()}}
    /* selector == 'term-filter' */
    if(selector == 'null'){ clear(); deactivate(); }
    jQuery.ajax({
        url : ajaxUrl,
        method : 'POST',
        data : {
            'action'    : 'ajax_sort',
            'offset'    : offset,
            'post-type' : postType,
            'taxonomy'  : taxSlug,
            'term-id'   : termId,
            'term-slug' : termSlug,
            'tmp-name'  : tmpName,
            'selector'  : selector
        },
        cache : false,
        success : function(i){
            /* selector == 'load-more' */
            if(selector != 'null'){
                offset = offset + 2;
                enableBtn();
                deactivate();
                $('.ajax .tax').append(i);
            }else{
                $('.ajax .tax').html(i);
            }
        },
        complete: function(){
            resizrPlugin();
        },
        error : function(e){
            console.log('Fail : ', e)
        }
    });
}
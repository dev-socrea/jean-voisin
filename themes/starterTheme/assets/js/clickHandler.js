(function($, root, undefined) {
	$(document).ready(function() {

        'use strict';
		$('.nav').addClass('col-12 p-0');
		$('#menu-main-navigation').addClass('col-12 p-0 d-flex');
		$('.nav').find(".menu-item").addClass('col-4 p-0');
		$('.sub-menu').find("li").addClass('col-12 p-0');

        // Object variables for event handlers
        var triggers = ({
            sneakyBtn : $('#sneaky-btn'),
            loadMore : $('.load-more button'),
            termFilters : $('.term-filter'),
             menuBtn : $('.menu-btn')/*,*/
            // Add here...
        });

        triggers.sneakyBtn.click(function() {
            $('body').toggleClass('sneaky-open');
        });

        triggers.termFilters.click(function(){
            var termId = $(this).attr('data-id'),
                termSlug = $(this).attr('data-slug'),
                countPosts = $(this).attr('count-posts');

            triggers.loadMore.attr('count-posts', countPosts);
            if(termId != undefined && termSlug != undefined){
                triggers.loadMore.attr({
                    'data-id': termId,
                    'data-slug': termSlug
                })
            }else{
                triggers.loadMore.attr({
                    'data-id': 'null',
                    'data-slug': 'null'
                })
            }
        });

        triggers.loadMore.on('click', function(e){
            e.preventDefault();
            $(this).prop('disabled', true);
        });

        triggers.menuBtn.click(function() {
            $("body").toggleClass("responsive");
            $(".nav-mobile").fadeToggle("slow");
            $(this).toggleClass('open');
            $(this).find("button").toggleClass('menu-open');

        });

    // ADD class anim with Delay
        $('.menu-btn').click(function() {
            if ( $('body').hasClass( "responsive" ) ) {
                $('.nav-mobile .menu-item').removeClass('animation-fade-out')
                var delay = 0;
                 $('.nav-mobile .menu-item').each(function() {
                   var $li = $(this);
                   setTimeout(function() {
                     $li.addClass('animation-fade-up');
                   }, delay+=100); // delay 100 ms
                 });
            }
            else {
                setTimeout(function() {
                    $('.nav-mobile .menu-item').removeClass('animation-fade-up');
                }, 800);

            }
        });
    });
})(jQuery, this);

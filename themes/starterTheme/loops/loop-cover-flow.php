<li class="item anim-500">
    <!-- article -->
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <!-- post thumbnail -->
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php if(has_post_thumbnail()){?>
                <?php the_post_thumbnail(); ?>
            <?php }else{ ?>
                <div style="height: 150px; background-size: contain; background-repeat: no-repeat; background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')"></div>
            <?php } ?>
        </a>
        <!-- /post thumbnail -->
        <?php //edit_post_link(); ?>
    </article>
    <!-- /article -->
</li>
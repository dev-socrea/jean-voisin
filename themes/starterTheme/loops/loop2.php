<?php query_posts('showposts=4'); if (have_posts()) : while (have_posts()) : the_post(); ?>

	<!-- article -->
	<div class="col-lg-4 col-md-6 col-xs-12">
	<article id="post-<?php the_ID(); ?>" <?php post_class("animation-fade-up"); ?>>


		<!-- post thumbnail -->
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php if(has_post_thumbnail()){ // Check if thumbnail exists ?>
					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
					<div  class="img-background" style="background-image: url('<?php echo $thumb['0'];?>')" no-repeat center fixed>
					</div>
        <?php }else{ ?>
            <div style="height: 500px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')"><div class="salut"></div></div>
        <?php } ?>
        </a>
		<!-- /post thumbnail -->

		<!-- post title -->
		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- /post title -->

		<!-- post details -->
		<span class="category"><?php the_category(', '); ?></span>
		<!-- <span class="date"><?php the_time('F j, Y'); ?></span>
		<span class="author"><?php _e( 'Published by', 'starterTheme' ); ?> <?php the_author_posts_link(); ?></span>
		<span class="comments-count"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'starterTheme' ), __( '1 Comment', 'starterTheme' ), __( '% Comments', 'starterTheme' )); ?></span> -->
		<!-- /post details -->

		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?></a>

		<!-- <?php edit_post_link(); ?> -->

	</article>
	</div>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>

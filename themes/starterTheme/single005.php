<?php get_header(); ?>
<div class="container-fluid fil-dariane">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 no-padding flex">
				<h1>
					<?php _e('Latest Posts', 'starterTheme'); ?>
				</h1>
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid nopadding">
  <div class="row">
    <main role="main" class="main-content nopadding">
      <!-- section -->
      <section>
          <?php if (have_posts()): while (have_posts()) : the_post(); ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class('single-poste-content'); ?> >
                  <div class="col-lg-6 col-md-6 col-xs-6 flex">
                    <div class="text-container m-l-auto">
                      <h1 class="title-actu-single"><?php the_title(); ?></h1>
                      <span>
                        <span><i class="fa fa-calendar-o"></i> <?php _e('Published on: ', 'starterTheme') ?></span>
                        <i><?php the_time('j F Y'); ?> </i>
                      </span>
                      <span><i class="fa fa-tag" aria-hidden="true"></i>
                        <?php
                        foreach((get_the_category()) as $category) {
                          if ($category->cat_name != 'slider') {
                            echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                          }
                        }?>
                      </span>
                      <?php the_content(); ?>
                      <?php social_media('google'); social_media('twitter'); social_media('facebook'); ?>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-6 no-padding p-t-200 min-h-90vh">
                    <!-- article -->
                    <?php if(has_post_thumbnail()){ // Check if thumbnail exists ?>
                      <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                      <div  class="single-post-thumb" style="background-image: url('<?php echo $thumb['0'];?>')">
                      </div>
                    <?php }else{ ?>
                      <div class="single-post-thumb" style="background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')"></div>
                    <?php } ?>
                  </div>
                </article>
              <?php // get_template_part('paginations/pagination', 'nextprev'); ?>
            <?php endwhile; ?>
          <?php else: ?>
            <!-- article -->
            <article>
              <h1><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h1>
            </article>
            <!-- /article -->
          <?php endif; ?>
        </div>
      </section>
      <!-- /section -->
    </main>
  </div>
</div>

<?php get_footer(); ?>

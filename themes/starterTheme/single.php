<?php get_header(); ?>
<div class="container">
    <div class="row m-t-50 fan">
        <div class="col-12">
          <?php if(has_post_thumbnail()){ // Check if thumbnail exists ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
            <div  class="single3-img" style="background-image: url('<?php echo $thumb['0'];?>')">
            </div>
            <?php }else{ ?>
            <div class="single3-img" style="background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')">
            </div>
        <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="right-effect col-6 text-container p-l-20 p-t-20 vertical-center direction-column">
            <div class="social_logo"><?php social_media('google'); social_media('twitter'); social_media('facebook'); ?></div>
            <h1 class="title-actu-single"><?php the_title(); ?></h1>
            <span class="info-article">
                <span><i class="fa fa-calendar-o"></i> <?php _e('Published on: ', 'starterTheme') ?></span>
                <i><?php the_time('j F Y'); ?> </i>
            </span>
            <span class="m-l-15 info-article"><i class="fa fa-tag" aria-hidden="true"></i>
                <?php
                foreach((get_the_category()) as $category) {
                  if ($category->cat_name != 'slider') {
                    echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                  }
                }?>
            </span>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="m-t-20">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="left-effect col-12 p-0">
            <div class="single3-img-container col-12 p-0">
                <div class="line-separator"></div>
                <?php
                if (get_field('gallery')) {
                  $image_ids = get_field('gallery', false, false);
                  $singlegallerythumb = '[gallery link="file" ids="' . implode(',', $image_ids) . '"]';
                  echo do_shortcode( $singlegallerythumb );
                }?>
            </div>
        </div>
    </div>
</div>

    <!-- <script type="text/javascript">
    $(window).scroll(function() {

      // NOMBRE DE PIXELS SCROLL
      var scroll = -($(window).scrollTop());

      // TAILLE DES DIVS GAUCHE ET DROITE AVEC EN + LA TAILLE DU HEADER
      var hl = ($(".left-effect").height() + $("#header-sticky").height());
      var hl2 = ($(".right-effect").height() + $("#header-sticky").height());

      // CALCULS DE LA DIFERENCE ENTRE LA TAILLE DE L'ECRAN ET DES DIVS
      var test = $( window ).height() - hl;
      var test2 = $( window ).height() - hl2;

      // DIFFERENCE ENTRE LA DIV DE GAUCHE ET LE SCROLL POUR AVOIR LE NOMBRE DE PIXEL A AJOUTER A LA DIV
      var up = (test - scroll);


      if (hl > $( window ).height()) { // NE PAS FAIRE L'ANIMATION SI LA DIV NE DEPASSE PAS LE VIEWPORT
        if (scroll <= test) { // DECLENCHEMENT QUAND LA DIV DE GAUCHE ARRIVE A LA FIN
          if (scroll >= test2) { // AFFICHER LE FOOTER QUAND LA DIV DE DROITE ATTEND LA FIN
            $('.left-effect').css({
              transform: "translate3d(0, "+up+"px, 0)" // AJOUT DE PIXEL A LA DIV DE GAUCHE POUR L'EFFET STATIC
            });
          }
        }
      }
    });
  </script> -->
  <?php get_footer(); ?>
